.PHONY: run

IMAGE=alanocallaghan/basicsworkflow2020-docker
VERSION=0.5.3

all: rds/sce_sm.Rds rds/sce_active.Rds rds/chain_active.Rds rds/chain_sm.Rds ExtendedMethods.pdf DataPreparationBASiCSWorkflow.pdf AnalysisCD4T.pdf

clean:
	rm rds/*.Rds
	rm *.pdf

run:
	docker run -v $(shell pwd):/home/rstudio/mycode \
		-w /home/rstudio/mycode \
		-v /tmp/.X11-unix:/tmp/.X11-unix:ro \
		-e DISPLAY=${DISPLAY} \
		-it $(IMAGE):$(VERSION) \
		/bin/bash

rds/chain_sm.Rds: rds/sce_sm.Rds
	docker run -v $(shell pwd):/home/rstudio/mycode \
		-w /home/rstudio/mycode \
		$(IMAGE):$(VERSION) \
		/bin/bash \
		-c "Rscript chains_mesoderm.R"

%.pdf: %.Rmd
	docker run -v $(shell pwd):/home/rstudio/mycode \
		-w /home/rstudio/mycode \
		$(IMAGE):$(VERSION) \
		/bin/bash \
		-c "Rscript -e 'rmarkdown::render(\"$<\")'"

AnalysisCD4T.Rmd: rds/chain_active.Rds
rds/chain_active.Rds: DataPreparationCD4T.pdf
rds/sce_active.Rds: DataPreparationCD4T.pdf
rds/sce_sm.Rds: DataPreparationBASiCSWorkflow.pdf
