# BASiCSWorkflow2020

Large files for the BASiCS workflow.
These are primarily MCMCs stored as .rds files.

To generate all PDF documents from each of the Rmd files, and all of the
Rds files used in these documents and in the main BASiCS workflow document,
first install Docker and GNU Make or a suitable
alternative. Then, from the root directory of this repository,
it should suffice to run the following command:

```bash
make
```
